<?php

namespace App\Http\Controllers;

use App\Http\Resources\ShowResource;
use App\Models\Ia7;
use Illuminate\Http\Request;


class Ia7Controller extends Controller
{
    public function show($fecha){
        $ia7 = Ia7::where('fecha',$fecha)->first();
        if(!$ia7){
            return response()->json([
                'errors' => Array([
                    'code' =>404,
                    'message'=>'No hay datos de Ia7'
                ])
            ],404);
        }
        /*return response()->json([
            'status' => 'ok',
            'data' => $ia7
        ],200);*/
        return new ShowResource($ia7);

    }

    public function showCollection($id, $id2)
    {
        if($id>$id2){
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'Fecha inicial superior a la final'])]);
        }
        $ia7 =DB::select(DB::raw("SELECT * FROM ia7 WHERE fecha BETWEEN '$id' and '$id2'"));
        if(!$ia7){
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No existe la fecha'])],404);
        }
     //dd($ia14);
        return new CovidCollection($ia7);
    }

    public function showAll(){
        $ia7 = Ia7::all();
        if(!$ia7){
            return response()->json([
                'errors' => Array([
                    'code' =>404,
                    'message'=>'No hay datos de Ia7'
                ])
            ],404);
        }
        return response()->json([
            'status' => 'ok',
            'data' => $ia7
            ],200);
    }

    public function ia7update(Request $request){
        $ia7 = new Ia7();
        $ia7->id = $request->id;
        $ia7->fecha = $request->fecha;
        $ia7->ccaas_id = $request->ccaas_id;
        $ia7->incidencia = $request->incidencia;
        $ia7Updated= Ia7::where('id',$request->id)->update($ia7);

        if ($ia7Updated) {

            return 'true';

        }else{

            return 'false';

        }
    }

    public function store(Request $request){

        $ia7 = new Ia7();
        $ia7->fecha= $request->fecha;
        $ia7->ccaas_id= $request->ccaas_id;
        $ia7->incidencia= $request->incidencia;
        $ia7->save();

        return response()->json($ia7);

    }
}
