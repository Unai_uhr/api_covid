<?php

namespace App\Http\Controllers;

use App\Models\Muertos;
use Illuminate\Http\Request;

class MuertosController extends Controller
{
    public function show($fecha){
        $casos = Muertos::where('fecha',$fecha)->first();
        if(!$casos){
            return response()->json([
                'errors' => Array([
                    'code' =>404,
                    'message'=>'No hay datos en la tabla Cason en esa fecha'
                ])
            ],404);
        }
        return response()->json([
            'status' => 'ok',
            'data' => $casos
        ],200);

    }

    public function showCollection($id, $id2)
    {
        if($id>$id2){
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'Fecha inicial superior a la final'])]);
        }
        $muertos =DB::select(DB::raw("SELECT * FROM muertos WHERE fecha BETWEEN '$id' and '$id2'"));
        if(!$muertos){
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No existe la fecha'])],404);
        }
     //dd($ia14);
        return new CovidCollection($muertos);
    }

    public function showAll(){
        $casos = Muertos::all();
        if(!$casos){
            return response()->json([
                'errors' => Array([
                    'code' =>404,
                    'message'=>'No hay datos de Casos ;'
                ])
            ],404);
        }
        return response()->json([
            'status' => 'ok',
            'data' => $casos
        ],200);
    }

    public function casosupdate(Request $request){
        $casos = new Muertos();
        $casos->id = $request->id;
        $casos->fecha = $request->fecha;
        $casos->ccaas_id = $request->ccaas_id;
        $casos->numero = $request->numero;
        $casosUpdated= Ia7::where('id',$request->id)->update($casos);

        if ($casosUpdated) {

            return 'true';

        }else{

            return 'false';

        }
    }


    public function store(Request $request){

        $casos = new Muertos();
        $casos->fecha= $request->fecha;
        $casos->ccaas_id= $request->ccaas_id;
        $casos->numero= $request->numero;
        $casos->save();

        return response()->json($casos);

    }
}
