<?php

namespace App\Http\Controllers;

use App\Http\Resources\Ia14Resource;
use App\Http\Resources\ShowResource;
use App\Models\Ia14;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\CovidCollection;

class Ia14Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    public function showAll()
    {
        $ia14 =Ia14::all();
        if(!$ia14){
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No hay datos'])],404);
        }
        return response()->json(['status'=>'ok','data'=>$ia14],200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ia14 = new Ia14();
        $ia14->fecha = $request->fecha;
        $ia14->ccaas_id = $request->ccaas_id;
        $ia14->incidencia = $request->incidencia;
        $ia14->save();
        return response()->json($ia14);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ia14 =Ia14::where('fecha',$id)->first();
        //$ia14 =whereDate('fecha','=',$id);
        if(!$ia14){
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No existe la fecha'])],404);
        }
        //return response()->json(['status'=>'ok','data'=>$ia14],200);
        return new ShowResource($ia14);
    }


    public function showCollection($id, $id2)
    {
        if($id>$id2){
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'Fecha inicial superior a la final'])]);
        }
        $ia14 =DB::select(DB::raw("SELECT * FROM ia14 WHERE fecha BETWEEN '$id' and '$id2'"));
        if(!$ia14){
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No existe la fecha'])],404);
        }
     //dd($ia14);
        return new CovidCollection($ia14);
    }

        /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ia14Update(Request $request)
    {
        $ia14 = new Ia14();
        $ia14->id = $request->id;
        $ia14->fecha = $request->fecha;
        $ia14->ccaas_id = $request->ccaas_id;
        $ia14->incidencia = $request->incidencia;
        $ia14Updated= Ia14::where('id',$request->id)->update($ia14);

        if ($ia14Updated) {

            return 'true';

        }else{

            return 'false';

        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
